import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateMenuComponent } from './create-update-menu/create-update-menu.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  { component: MenuComponent, path: ''},
  { component: CreateUpdateMenuComponent, path: 'createUpdate'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
