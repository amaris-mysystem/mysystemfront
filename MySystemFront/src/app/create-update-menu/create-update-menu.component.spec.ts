import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateMenuComponent } from './create-update-menu.component';

describe('CreateUpdateMenuComponent', () => {
  let component: CreateUpdateMenuComponent;
  let fixture: ComponentFixture<CreateUpdateMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateUpdateMenuComponent]
    });
    fixture = TestBed.createComponent(CreateUpdateMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
