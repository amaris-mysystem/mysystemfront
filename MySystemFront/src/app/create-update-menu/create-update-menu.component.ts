import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { Menu } from '../model/menu';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-update-menu',
  templateUrl: './create-update-menu.component.html',
  styleUrls: ['./create-update-menu.component.css']
})
export class CreateUpdateMenuComponent implements OnInit {

  name: string | undefined;
  cookingRecipe: string | undefined;
  calories: number | undefined;
  idMenu: number | undefined;

  constructor(private menuService: MenuService, private activatedRoute: ActivatedRoute, private router: Router){}
  
  ngOnInit(): void {
    let urlTree = this.router.parseUrl(this.router.url);
    this.idMenu = urlTree.queryParams['id'];
    if(this.idMenu != null){
      this.loadMenu(this.idMenu);
    } 
  }

  createMenu(){
    const newMenu: Menu = {
      idMenu: 0,
      name: this.name,
      cookingRecipe: this.cookingRecipe,
      calories: this.calories
    };
    this.menuService.createMenu(newMenu).subscribe(()=>{
      this.router.navigateByUrl('');
    });
  }

  updateMenu(){
    const menu: Menu = {
      idMenu: 0,
      name: this.name,
      cookingRecipe: this.cookingRecipe,
      calories: this.calories
    };
    this.menuService.updateMenu(this.idMenu, menu).subscribe(()=>{
      this.router.navigateByUrl('');
    });
  }

  loadMenu(id: number){
    this.menuService.getOneMenuById(id).subscribe(item=> {
      this.name = item.name,
      this.cookingRecipe = item.cookingRecipe,
      this.calories = item.calories
    });
  }
}
