import { Component, OnInit } from '@angular/core';
import { Menu } from '../model/menu';
import { MenuService } from '../services/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menus: Menu[] | undefined;

  constructor(private menuSservice: MenuService, private router: Router){}
  ngOnInit(): void {
    this.loadMenus();
  }

  loadMenus(){
    this.menuSservice.getManyMenu().subscribe(items=> {
      this.menus = items;
    });
  }

  deleteMeunu(id: number){
    this.menuSservice.delete(id).subscribe(()=>{
      this.loadMenus();
    });
  }

  updateItem(idMenu: number){
    this.router.navigateByUrl('createUpdate?id='+idMenu);
  }

  createItem(){
    this.router.navigate(['createUpdate']);
  }
}
