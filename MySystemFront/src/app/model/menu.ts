export interface Menu {
    idMenu: number 
    name: string | undefined
    cookingRecipe: string | undefined
    calories: number | undefined
}