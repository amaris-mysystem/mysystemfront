import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Menu } from '../model/menu';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  url = 'https://localhost:7241/api/Menu';
  constructor(private http: HttpClient) { }

  getManyMenu(): Observable<any[]> {
    console.log(this.http.get<any[]>(this.url));
    return this.http.get<any[]>(this.url);
  }

  getOneMenuById(id: number): Observable<any> {
    return this.http.get<any>(this.url + `/${id}`);
  }

  createMenu(item: Menu): Observable<any> {
    return this.http.post<any>(this.url, item);
  }

  updateMenu(id: number | undefined, item: Menu): Observable<any> {
    return this.http.put<any>(this.url + `/${id}`, item);
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(this.url + `/${id}`);
  }
}
